---
author: Mathieu Buchwald
title: Maths expertes +
tags:
  - Maths expertes
  - approfondissements
---

# Approfondissements 

## 1. Complexes

## 2. Matrices

## 3. Arithmétique
  

!!! note "Fichiers à télécharger"
    | Thème  | Enoncé | Solution |
    | :---:    | :----:    | :---:   |
    | Irrationnalité de $\sqrt p$   | <a href="https://forge.apps.education.fr/buchwald/whereismymath/-/raw/main/docs/math_expertes/arithmetique/racine_de_p.pdf" target="_blank"> <img alt="Enoncé" src="https://forge.apps.education.fr/buchwald/whereismymath/-/raw/main/img/logo_perso/logo_exo_2.png"  />  </a> | <a href="https://forge.apps.education.fr/buchwald/whereismymath/-/raw/main/docs/math_expertes/arithmetique/racine_de_p_sol.pdf" target="_blank">  <img alt="Solution" src="https://forge.apps.education.fr/buchwald/whereismymath/-/raw/main/img/logo_perso/logo_sol_2.png"  />  </a>|
    | Algorithme d'Euclide étendu  | <a href="https://forge.apps.education.fr/buchwald/whereismymath/-/raw/main/docs/math_expertes/arithmetique/euclide_etendu.pdf" target="_blank"> <img alt="Enoncé" src="https://forge.apps.education.fr/buchwald/whereismymath/-/raw/main/img/logo_perso/logo_exo_2.png"  />  </a> | <a href="https://forge.apps.education.fr/buchwald/whereismymath/-/raw/main/docs/math_expertes/arithmetique/euclide_etendu_sol.pdf" target="_blank" >  <img alt="Solution" src="https://forge.apps.education.fr/buchwald/whereismymath/-/raw/main/img/logo_perso/logo_sol_2.png"  />  </a>|    



# Prolongements

## 1. Complexes

## 2. Matrices

## 3. Arithmétique

## 4. Algos




