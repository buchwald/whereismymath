---
author: Mathieu Buchwald
title: 🏡 Accueil
---

# Des ressources en maths

!!! info "Ce modèle de site"

    😊 Ce site est actuellement en cours de création.

   J'y mettrai des compléments de cours pour les lycéens niveau terminales. Le lecteur curieux pourra y trouver des prolongements de cours. Les étudiants en mathématiques ou les autodidactes pourront aussi y trouver leur bonheur.
   Les enseignants pourront aussi y trouver, je l'espère des choses intéressantes. 


## Ce que vous trouverez sur ce site

Il y aura des fichiers pdf à télécharger, des fichiers sources en Latex, ainsi que quelques scripts en Pythons.



## Types de contenus

Sur ce site, il y aura 

👉 Des compléments de cours : 

- pour les terminales de la spécialité maths

- pour les terminales de l'option maths expertes


👉 Des maths pour aller plus loin (niveau post-bac) 

