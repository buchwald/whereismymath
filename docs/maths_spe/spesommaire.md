---
author: Mathieu Buchwald
title: Spé +
tags:
  - spécialité maths
  - approfondissements
---

# I. Approfondissement du cours

## Analyse

### 1. Les suites

#### Suites arithmétiques

??? note "Vidéo"
    <iframe width="560" height="315" src="https://www.youtube.com/embed/wRzJgzUdqpk?si=CHCj1HUvedlvI_Me" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
    



#### Suites géométriques

??? note "Vidéo"
    <iframe width="560" height="315" src="https://www.youtube.com/embed/2Wi3H7SY4Nw?si=G1Dtj9YiOTaon7ZO" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
    <iframe width="560" height="315" src="https://www.youtube.com/embed/5Lj3G8Ri7Wg?si=tMW3WjdI4buHpXfK" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
    <iframe width="560" height="315" src="https://www.youtube.com/embed/-gtch8nQ9EU?si=2o-VEGNxcbhVwy2H" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>




### 2. Les limites

#### Limites de suites

??? note "Vidéo"
    <iframe width="560" height="315" src="https://www.youtube.com/embed/LwqA9VM8gY4?si=4So_ebfhvPMGFIJf" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>



### 3. La continuité

## Géométrie

## Dénombrement et probabilités

### 1. Dénombrement

??? note "Démonstrations"
    - [Démonstration du théorème du binome de Newton](https://forge.apps.education.fr/buchwald/whereismymath/-/raw/main/docs/maths_spe/denombrement/Preuves/dem_binome_Newton.pdf?ref_type=heads&inline=false){ .md-button target="_blank" rel="noopener" }
    

####  

## II. Prolongements du cours

### 1. Suites



