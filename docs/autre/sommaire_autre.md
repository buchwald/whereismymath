---
author: Mathieu Buchwald
title: Autre
tags:
  - Maths expertes
  - approfondissements
---

# Cours

## I. Algèbre :


### 1. Ensembles

### 2. Structures

### 3. Les entiers naturels

### 4. Arithmétique

??? note "A télécharger" 

  | Thème  | Enoncé | Solution |
  | :---:    | :----:    | :---:   |
  | Irrationnalité de $\sqrt p$   | <a href="https://forge.apps.education.fr/buchwald/whereismymath/-/raw/main/docs/math_expertes/arithmetique/racine_de_p.pdf" target="_blank">E</a>  | <a href="https://forge.apps.education.fr/buchwald/whereismymath/-/raw/main/docs/math_expertes/arithmetique/racine_de_p_sol.pdf" target="_blank">E</a>|


  

## II. Analyse :


### 1. Suites

### 2. Limites

### 3. Fonctions







